package Model

import java.io.File
import java.io.FileNotFoundException

class ModoProfesor {
    val reset = "\u001B[0m"
    val lila = "\u001B[35m"
    val azul = "\u001B[34m"
    fun addProblem(titulo: String, explicacion: String, seccion: String, id:Int, numDeIntentos:Int, resuelto:Boolean, numDeEntradas: Int, jpp: List<String>, respuestas: List<String>) {
        DbToKotlin().postProblem(titulo, explicacion, seccion, id, numDeIntentos, resuelto, numDeEntradas, jpp, respuestas)
    }

    fun takeStudentReport(): String{
        var report = ""
        report+= "\n${lila}Reporte del estudiante$reset\n"
        val numDeResueltos = DbToKotlin().getNumDeResueltos()
        val numIntentosTotal = DbToKotlin().getNumDeIntentosTotal()
        val totalDeProblemas = Game().numDeProblemas()
        if(numIntentosTotal == 0){
            return "\nTodaviá no hay datos del alumno..."
        }
        report+="${azul}Problemas resueltos:$reset $numDeResueltos/${totalDeProblemas}\n"
        report+="${azul}Intentos totales:$reset $numIntentosTotal\n"
        val nota: Double = (numDeResueltos.toDouble()/numIntentosTotal.toDouble())*10.0
        report+="${azul}Nota final:$reset $nota/10 (Nota sobre numero de problemas resueltos)"
        return report
    }

    fun txtToKotlinString(path: String): String{
        try {
            val texto = File(path).readText()
            return texto
        } catch (e: FileNotFoundException) {
            return ""
        }
    }
}