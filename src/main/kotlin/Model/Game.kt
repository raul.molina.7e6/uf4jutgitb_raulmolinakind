package Model

class Game(){
    fun getSiguienteProblema(): Int{
        for(i in 1..numDeProblemas()){
            if(!DbToKotlin().getResuelto(i)){
                return i
            }
        }
        return 0
    }
    //Funcion para saber cuantos problemas hay
    fun numDeProblemas(): Int{
        return DbToKotlin().getNumDeProblemas()
    }
}