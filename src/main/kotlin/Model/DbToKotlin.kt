package Model

import java.sql.DriverManager

class DbToKotlin(){
    //Conexion a la base de datos
    val dataBase = "jdbc:postgresql://localhost:5432/jutgitb"
    val conexion = DriverManager.getConnection(dataBase, "postgres", "3124")

    //TEXTO SIMPLE
    fun getIntro(): String{
        var textoRetorno = ""
        val statement = conexion.createStatement()
        val query = "SELECT texto FROM TextoSimple where tipo='intro'"
        val result = statement.executeQuery(query)
        while (result.next()){
            textoRetorno = result.getString("texto")
        }
        conexion.close()
        return textoRetorno
    }
    fun getHelp(): String{
        var textoRetorno = ""
        val statement = conexion.createStatement()
        val query = "SELECT texto FROM TextoSimple where tipo='help'"
        val result = statement.executeQuery(query)
        while (result.next()){
            textoRetorno = result.getString("texto")
        }
        conexion.close()
        return textoRetorno
    }

    //PROBLEMAS
    fun getTitulo(numEnunciado: Int): String{
        var infoReturn = ""
        val statement = conexion.createStatement()
        val query = "SELECT titulo FROM problema where id=$numEnunciado"
        val result = statement.executeQuery(query)
        while (result.next()){
            infoReturn = result.getString("titulo")
        }
        conexion.close()
        return infoReturn
    }
    fun getEnunciado(numEnunciado: Int): String{
        var infoReturn = ""
        val statement = conexion.createStatement()
        val query = "SELECT explicacion FROM problema where id=$numEnunciado"
        val result = statement.executeQuery(query)
        while (result.next()){
            infoReturn = result.getString("explicacion")
        }
        conexion.close()
        return infoReturn
    }

    fun getSeccion(numEnunciado: Int): String{
        var infoReturn = ""
        val statement = conexion.createStatement()
        val query = "SELECT seccion FROM problema where id=$numEnunciado"
        val result = statement.executeQuery(query)
        while (result.next()){
            infoReturn = result.getString("seccion")
        }
        conexion.close()
        return infoReturn
    }

    fun getNumDeIntentos(numEnunciado: Int): Int{
        var infoReturn = 0
        val statement = conexion.createStatement()
        val query = "SELECT numDeIntentos FROM problema where id=$numEnunciado"
        val result = statement.executeQuery(query)
        while (result.next()){
            infoReturn = result.getInt("numDeIntentos")
        }
        conexion.close()
        return infoReturn
    }

    fun getResuelto(numEnunciado: Int): Boolean{
        var infoReturn = false
        val statement = conexion.createStatement()
        val query = "SELECT resuelto FROM problema where id=$numEnunciado"
        val result = statement.executeQuery(query)
        while (result.next()){
            infoReturn = result.getBoolean("resuelto")
        }
        conexion.close()
        return infoReturn
    }

    fun getNumDeEntradas(numEnunciado: Int): Int{
        var infoReturn = 0
        val statement = conexion.createStatement()
        val query = "SELECT numDeEntradas FROM problema where id=$numEnunciado"
        val result = statement.executeQuery(query)
        while (result.next()){
            infoReturn = result.getInt("numDeEntradas")
        }
        conexion.close()
        return infoReturn
    }

    fun getJpp(numEnunciado: Int): List<String>{
        val query = "SELECT preguntas FROM JPP where id=$numEnunciado"
        val statement = conexion.createStatement()
        val result = statement.executeQuery(query)
        var jpp = ""
        while (result.next()){
            jpp = result.getString("preguntas")
        }
        conexion.close()
        return toList(jpp)
    }

    fun getRespuesta(numEnunciado: Int): List<String>{
        val query = "SELECT respuestas FROM RESPUESTA where id=$numEnunciado"
        val statement = conexion.createStatement()
        val result = statement.executeQuery(query)
        var respuestas = ""
        while (result.next()){
            respuestas = result.getString("respuestas")
        }
        conexion.close()
        return toList(respuestas)
    }

    fun getNumDeResueltos(): Int{
        var contador = 0
        val statement = conexion.createStatement()
        val query = "SELECT * FROM problema"
        val result = statement.executeQuery(query)
        while (result.next()){
            val resuelto = result.getBoolean("resuelto")
            if(resuelto) contador++
        }
        conexion.close()
        return contador
    }
    fun getNumDeIntentosTotal(): Int{
        var contador = 0
        val statement = conexion.createStatement()
        val query = "SELECT * FROM problema"
        val result = statement.executeQuery(query)
        while (result.next()){
            val resuelto = result.getBoolean("resuelto")
            val intentos = result.getInt("numDeIntentos")
            if(resuelto) contador+=intentos
        }
        conexion.close()
        return contador
    }
    private fun toList(pasarAlista: String): List<String>{
        val listReturn = mutableListOf<String>()
        for(i in pasarAlista.split("=")){
            listReturn.add(i)
        }
        return listReturn
    }
    fun getNumDeProblemas(): Int{
        var respuesta = 0
        val statement = conexion.createStatement()
        val query = "SELECT * FROM problema"
        val result = statement.executeQuery(query)
        while (result.next()) respuesta++
        return respuesta
    }
    fun putProblemaResuelto(idProblema: Int, numDeIntentos: Int){
        //Cambiar a true y guardar numero de intentos
        val intentos = DbToKotlin().getNumDeIntentos(idProblema) + numDeIntentos
        val statement = conexion.createStatement()
        val query = "UPDATE problema SET resuelto=true where id=$idProblema"
        statement.executeUpdate(query)
        val query2 = "UPDATE problema SET numDeIntentos=$intentos where id=$idProblema"
        statement.executeUpdate(query2)
    }

    //Añadir problema
    fun postProblem(titulo: String, explicacion: String, seccion: String, id:Int, numDeIntentos:Int, resuelto:Boolean, numDeEntradas: Int, jpp: List<String>, respuestas: List<String>){
        val query = "INSERT INTO problema VALUES (?,?,?,?,?,?,?)"
        val statement = conexion.prepareStatement(query)
        statement.setString(1, titulo)
        statement.setString(2, explicacion)
        statement.setString(3, seccion)
        statement.setInt(4, id)
        statement.setInt(5, numDeIntentos)
        statement.setBoolean(6, resuelto)
        statement.setInt(7, numDeEntradas)
        statement.executeUpdate()

        val statementInserts = conexion.createStatement()

        val query2 = "INSERT INTO jpp VALUES ($id, '${listaToString(jpp)}')"
        statementInserts.executeUpdate(query2)

        val query3 = "INSERT INTO respuesta VALUES ($id, '${listaToString(respuestas)}')"
        statementInserts.executeUpdate(query3)
    }
    private fun listaToString(lista: List<String>): String{
        var infoReturn = ""
        for(i in lista){
            infoReturn+=i
            infoReturn+="="
        }
        return infoReturn
    }

}