import Model.Game
import Model.DbToKotlin
import Model.ModoProfesor
import java.util.Scanner
val scanner = Scanner(System.`in`)
val reset = "\u001B[0m"
val lila = "\u001B[35m"
val azul = "\u001B[34m"
val rojo = "\u001B[31m"
val verde = "\u001B[32m"
val cyan = "\u001B[36m"

fun main(){
    println(DbToKotlin().getIntro())
    while (true){
        println("\nMENU PRINCIPAL\n")
        println("1- Seguir amb l’itinerari d’aprenentatge\n" +
                "2- Llista problemes\n" +
                "3- Consultar històric de problemes\n" +
                "4- Ajuda\n" +
                "5- Modo profesor\n" +
                "0- Sortir\n")

        val start = scanner.nextInt()
        if(start == 1){
            siguienteNivel()
        }else if(start == 2){
            listaDeProblemas()
        }else if(start == 3){
            historicProblems()
        }else if(start == 4){
            println(DbToKotlin().getHelp())
        }else if(start == 5){
            modoProfesor()
        }else if(start == 0){
            kotlin.system.exitProcess(0)
        }else{
            println("\nEsa opción no existe\n")
        }
    }
}
fun siguienteNivel(){
    val idSiguienteProblema = Game().getSiguienteProblema()
    if(idSiguienteProblema == 0){
        println("\nYa has resuelto todos los problemas disponibles")
    }else{
        val nombreSiguienteNivel = DbToKotlin().getTitulo(idSiguienteProblema)
        println("El siguiente nivel es $lila$nombreSiguienteNivel$reset")
        println("\nNumero:$azul $idSiguienteProblema $reset")
        while (true){
            println("\n¿Quieres jugarlo?")
            println("Si\nNo(Salir)")
            val entrada = scanner.next().lowercase()
            if (entrada == "si"){
                jugarProblema(idSiguienteProblema)
                break
            }else if(entrada == "no"){
                break
            }else{
                println("\nEsa opción no existe\n")
            }
        }
    }
}
fun listaDeProblemas(){
    println("\nLLISTA DE PROBLEMES")
    var contadorProblema = 1
    while (true){
        val nombreSiguienteNivel = DbToKotlin().getTitulo(contadorProblema)
        val resueltoONo = DbToKotlin().getResuelto(contadorProblema)

        println("\nNombre: $lila$nombreSiguienteNivel$reset")
        println("Numero:$azul $contadorProblema $reset")
        if(resueltoONo){
            println("Estado:$verde RESUELTO $reset")
        }else{
            println("Estado:$rojo NO RESUELTO $reset")
        }
        println("\n1- Siguiente\n" +
                "2- Jugar problema\n" +
                "0- Salir")
        val entrada = scanner.next()
        if (entrada == "1"){
            contadorProblema++
        }else if(entrada == "0")break
        else if(entrada == "2")jugarProblema(contadorProblema)
        else println("\nEsa opcion no existe")
        if(contadorProblema == Game().numDeProblemas() +1) contadorProblema=1
    }
}
fun jugarProblema(IdProblema: Int){
    var numDeIntentos = 0
    println(DbToKotlin().getEnunciado(IdProblema))
    println("\nJoc de proves privat:")
    val listaJpp = DbToKotlin().getJpp(IdProblema)
    for (i in listaJpp) println(i)
    var entrada1: String
    var entrada2: String
    var entrada3: String
    val numDeEntradas = DbToKotlin().getNumDeEntradas(IdProblema)
    val respuestas = DbToKotlin().getRespuesta(IdProblema)
    while (true){
        numDeIntentos++
        println("Respuesta:\n")
        if(numDeEntradas == 1){
            entrada1 = scanner.next()
            if(entrada1 == respuestas[0]) break
            else println("Respuesta incorrecta")
        }else if(numDeEntradas == 2){
            entrada1 = scanner.next()
            entrada2 = scanner.next()
            if(entrada1 == respuestas[0] && entrada2 == respuestas[1]) break
            else println("Respuesta incorrecta")
        }else if(numDeEntradas == 3){
            entrada1 = scanner.next()
            entrada2 = scanner.next()
            entrada3 = scanner.next()
            if(entrada1 == respuestas[0] && entrada2 == respuestas[1] && entrada3 == respuestas[2]) break
            else println("Respuesta incorrecta")
        }
    }
    println("¡Respuesta correcta!\nProblema resuelto")
    DbToKotlin().putProblemaResuelto(IdProblema, numDeIntentos)
}


fun historicProblems(){
    println("\nHISTORIC DE PROBLEMES\n")
    var contadorProblema = 1
    while (true){
        val nombreSiguienteNivel = DbToKotlin().getTitulo(contadorProblema)
        val numDeIntentos = DbToKotlin().getNumDeIntentos(contadorProblema)
        val resueltoONo = DbToKotlin().getResuelto(contadorProblema)

        println("Nombre: $lila$nombreSiguienteNivel$reset")
        println("Numero:$azul $contadorProblema $reset")
        println("Intentos:$cyan $numDeIntentos $reset")
        if(resueltoONo){
            println("Estado:$verde RESUELTO $reset")
        }else{
            println("Estado:$rojo NO RESUELTO $reset")
        }
        println("\n1- Siguiente\n" +
                "0- Salir\n")
        val entrada = scanner.next()
        if (entrada == "1"){
            contadorProblema++
        }else if(entrada == "0")break
        else println("\nEsa opcion no existe")
        if(contadorProblema == Game().numDeProblemas() +1) contadorProblema=1
    }
}
fun modoProfesor(){
    while(true){
        println("\n0- Salir\nContraseña:\n")
        val entrada = scanner.next().lowercase()
        if(entrada == "admin"){
            while (true){
                println("\n1- Añadir problema\n" +
                        "2- Reporte de alumno\n" +
                        "0- Cerrar sesión\n")
                val opcion = scanner.next()
                if(opcion == "1"){
                    println("Titulo:")
                    val titulo = scanner.next()
                    println("Explicación, Path del archivo.txt que contiene la explicaión (¡¡Joc de proves public també!!):")
                    val path = scanner.next()
                    println(path)
                    val explicacion = ModoProfesor().txtToKotlinString(path)

                    println("Sección (TDD (Tipus de dades), Condicionals, Bucles, Llistes,...):")
                    val seccion = scanner.next()
                    val id = Game().numDeProblemas()+1
                    val numDeIntentos = 0
                    val resuelto = false
                    println("¿Cuantas entradas tiene? 1, 2 o 3:")
                    val numDeEntradas = scanner.nextInt()
                    println("Joc de proves privat: (Escribelos seguidos en una linea separados por un espacio)")
                    val jpp = mutableListOf<String>()
                    for(i in 1..numDeEntradas){
                        val jocDeProvesPrivat = scanner.next()
                        jpp.add(jocDeProvesPrivat)
                    }
                    println("Respuestas: (Escribelas seguidas en una linea separadas por un espacio)")
                    val respuestas = mutableListOf<String>()
                    for(i in 1..numDeEntradas){
                        val respuestasALista = scanner.next()
                        respuestas.add(respuestasALista)
                    }
                    if(explicacion == ""){
                        println("\nNo se ha encontrado el archivo.txt especificado")
                    }else{
                        ModoProfesor().addProblem(titulo, explicacion, seccion, id, numDeIntentos, resuelto, numDeEntradas, jpp, respuestas)
                    }
                }else if(opcion =="2"){
                    println(ModoProfesor().takeStudentReport())
                }else if(opcion == "0"){
                    break
                }else{
                    println("Esa opción no existe")
                }
            }
        }else if(entrada == "0"){
            break
        }else{
            println("Esa no es la contraseña")
        }
    }
}